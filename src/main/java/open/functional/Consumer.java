package open.functional;

public interface Consumer<X> {
    void accept(X value);
}
