package open.functional.io;


import open.functional.Consumer;
import open.functional.FunctionThatCanThrow;
import open.functional.ProcessThatCanThrow;

public class IO<X, Y> {
    ProcessThatCanThrow<X, Y> process;

    public IO(ProcessThatCanThrow<X, Y> process) {
        this.process = process;
    }

//    As of now, not sure which form will this take
//    public static <A,B> IO<A,B> of(ProcessThatCanThrow<A,B> process) {
//        return new IO<>(process);
//    }

    public static <A, B> IO<A, B> of(FunctionThatCanThrow<A, B> f) {
        return new IO<A, B>((value, onSuccess, onFailure) -> {
            try {
                onSuccess.accept(f.transform(value));
            } catch (Exception e) {
                onFailure.accept(e);
            }

        });
    }

    public <A, B> IO<X, B> map(FunctionThatCanThrow<Y, B> f) {
        return new IO<X, B>((value, onSuccess, onFailure) -> {
            process(value, v -> {
                try {
                    onSuccess.accept(f.transform(v));
                } catch (Exception e) {
                    onFailure.accept(e);
                }
            }, onFailure);
        });
    }

    public <Z> IO<X, Z> chain(IO<Y, Z> io) {
        return new IO<X, Z>((value, onSuccess, onFailure) -> {
            process(
                    value,
                    processedY -> io.process(processedY, onSuccess, onFailure),
                    onFailure
            );

        });
    }

    public void process(X value, Consumer<Y> onSuccess, Consumer<Exception> onFailure) {
        process.process(value, onSuccess, onFailure);
    }

}
