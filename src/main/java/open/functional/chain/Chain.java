package open.functional.chain;


import open.functional.FunctionThatCanThrow;
import open.functional.Nothing;

public class Chain<X, Y> {
    public final FunctionThatCanThrow<X, Y> f;

    private Chain(FunctionThatCanThrow<X, Y> function) {
        this.f = function;
    }

    public static <Z> Chain<Nothing, Z> of(Z value) {
        return new Chain<>(ignore -> value);
    }

    public <Z> Chain<X, Z> map(FunctionThatCanThrow<Y, Z> g) {
        return new Chain<>(x -> g.transform(f.transform(x)));
    }

    public Y computeWith(X value) throws Exception {
        return f.transform(value);
    }

    public Y computeWith(X value, Y defaultValue) {
        try {
            return f.transform(value);
        } catch (Exception e) {
            return defaultValue;
        }
    }
}
