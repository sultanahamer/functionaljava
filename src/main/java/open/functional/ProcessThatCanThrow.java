package open.functional;


public interface ProcessThatCanThrow<X, Y> {
    void process(X value, Consumer<Y> onSuccess, Consumer<Exception> onFailure);
}
