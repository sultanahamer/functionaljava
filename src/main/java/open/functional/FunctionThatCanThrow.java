package open.functional;

public interface FunctionThatCanThrow<Y, Z> {
    Z transform(Y value) throws Exception;

    default Z transformWithDefault(Y value, Z defaultValue) {
        try {
            return transform(value);
        } catch (Exception e) {
            return defaultValue;
        }

    }
}
